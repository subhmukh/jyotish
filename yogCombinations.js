'use strict';
var calc = require('./chartSignCalc.js');
//install node js locally
//OR
//run online at https://replit.com/languages/nodejs

const calculate = (chart) => {
    const yogsFound = new Map();
    var chartObj = calc.getChartObject(chart);
    var disp = chartObj.disposition; //console.log(disp);
    var assocs = chartObj.allAssociations; //console.log(assocs);
    var hzsMap = chartObj.planetHousesMap; //console.log(hzsMap);
    for (const rule in yogList) {
        var title = yogList[rule].title;
        var p1 = Array.isArray(yogList[rule].planets) ? yogList[rule].planets[0] : (yogList[rule].planets != undefined ? yogList[rule].planets : null); 
        var p2 = (Array.isArray(yogList[rule].planets) && yogList[rule].planets.length === 2) ? yogList[rule].planets[1] : null;
        var disposition = yogList[rule].disposition != undefined && Array.isArray(yogList[rule].disposition) ? yogList[rule].disposition : null;
        var ruleElementPassed = 0;
        var mismatch = [];
        var enhanced = 0;
        var el2pass = Object.keys(yogList[rule]).filter(k => !["title", "planets", "enhanceif"].includes(k)).length;
        console.log("---" + yogList[rule].title + "[" + Object.keys(yogList[rule]) + "] - " + el2pass + " rules to pass");
        var p1c = [];
        var p2c = [];
        Object.keys(yogList[rule]).forEach((el) => {
            switch (el) {
                case "planetsByDisposition":
                    Object.entries(disp).forEach(([key, value]) => {
                        var anyOf1, anyOf2;
                        anyOf1 = anyOf2 = false;
                        if (!Array.isArray(yogList[rule].planetsByDisposition) && disp.get(key).includes(yogList[rule].planetsByDisposition)) anyOf1 = true;
                        else if (!Array.isArray(yogList[rule].planetsByDisposition[0]) && disp.get(key).includes(yogList[rule].planetsByDisposition[0])) anyOf1 = true;
                        else if (Array.isArray(yogList[rule].planetsByDisposition[0])) yogList[rule].planetsByDisposition[0].forEach((pde) => anyOf1 = anyOf1 || disp.get(key).includes(pde));
                        if (anyOf1) p1c.push(key);
                        if (Array.isArray(yogList[rule].planetsByDisposition) && yogList[rule].planetsByDisposition.length === 2) {
                            if (!Array.isArray(yogList[rule].planetsByDisposition[1]) && disp.get(key).includes(yogList[rule].planetsByDisposition[1])) anyOf2 = true;
                            else if (Array.isArray(yogList[rule].planetsByDisposition[1])) yogList[rule].planetsByDisposition[1].forEach((pde) => anyOf2 = anyOf2 || disp.get(key).includes(pde));
                            if (anyOf2) p2c.push(key);
                        }
                    });
                    p1 = (p1c.length > 1) ? p1c[0] : null;
                    p2 = (p2c.length > 1) ? p2c[0] : null;
                    if (p1 != null && p2 != null) ruleElementPassed++;
                    else if (p1 != null) mismatch.push("> Found " + p1 + " as '" + yogList[rule].planetsByDisposition[0] + " but not another with '" + yogList[rule].planetsByDisposition[1] + "']");
                    else if (p2 != null) mismatch.push("> Found " + p2 + " as '" + yogList[rule].planetsByDisposition[1] + " but not first planet with '" + yogList[rule].planetsByDisposition[0] + "']");
                    else mismatch.push("> Did not find 2 planets with disposition '" + yogList[rule].planetsByDisposition[0] + "' and '" + yogList[rule].planetsByDisposition[1] + "']");
                    break;
                case "inHouse":
                    if (yogList[rule].inHouse.includes(hzsMap.get(p1))) ruleElementPassed++;
                    else mismatch.push("> Incorrect position failed - [" + p1 + " in " + hzsMap.get(p1) + " and not in '" + yogList[rule].inHouse + "']");
                    break;
                case "disposition":
                    var anyOf = false;
                    if (Array.isArray(yogList[rule].disposition)) yogList[rule].disposition.forEach((de) => anyOf = anyOf || disp.get(p1).includes(de));
                    else if (disp.get(p1).includes(yogList[rule].disposition)) anyOf = true;
                    if (anyOf) ruleElementPassed++;
                    else mismatch.push("> Incorrect disposition failed - [" + p1 + " is '" + disp.get(p1) + "' and not any of '" + yogList[rule].disposition + "']");
                    break;
                case "aspect":
                    if ((p1 != null && p2 != null) || (p1c.length > 1 && p2c.length > 1)) {
                        var anyOf = false;
                        if (p1c.length > 1 && p2c.length > 1) {
                            p1c.forEach((ep1c) => {
                                p2c.forEach((ep2c) => {
                                    var assoc = (assocs.has(ep1c + "-" + ep2c)) ? assocs.get(ep1c + "-" + ep2c) : (assocs.has(ep2c + "-" + ep1c) ? assocs.get(ep2c + "-" + ep1c) : null);
                                    if (assoc != null && Array.isArray(yogList[rule].aspect)) yogList[rule].aspect.forEach((ae) => anyOf = anyOf || assoc.includes(ae));
                                    else if (assoc != null && assoc.includes(yogList[rule].aspect)) anyOf = true;
                                });
                            });
                            if (anyOf) ruleElementPassed++;
                            else mismatch.push("> Could not find required aspect '" + yogList[rule].aspect + "'");
                        } else {
                            var assoc = (assocs.has(p1 + "-" + p2)) ? assocs.get(p1 + "-" + p2) : (assocs.has(p2 + "-" + p1) ? assocs.get(p2 + "-" + p1) : null);
                            if (assoc != null && Array.isArray(yogList[rule].aspect)) yogList[rule].aspect.forEach((ae) => anyOf = anyOf || assoc.includes(ae));
                            else if (assoc != null && assoc.includes(yogList[rule].aspect)) anyOf = true;
                            if (anyOf) ruleElementPassed++;
                            else mismatch.push("> Incorrect aspect between " + p1 + " and " + p2 + "[Is '" + assoc + "' and not any of '" + yogList[rule].aspect + "']");
                        }
                    }
                    break;
                case "enhanceif":
                    Object.keys(yogList[rule].enhanceif).forEach((eni) => {
                        switch (eni) {
                            case "disposition":
                                var anyOf = false;
                                if (Array.isArray(yogList[rule].enhanceif.disposition)) yogList[rule].enhanceif.disposition.forEach((de) => anyOf = anyOf || disp.get(p1).includes(de));
                                else if (disp.get(p1).includes(yogList[rule].enhanceif.disposition)) anyOf = true;
                                if (anyOf) enhanced++;
                                else mismatch.push("> WEAK, NOT ENHANCED disposition - [" + p1 + " is '" + disp.get(p1) + "' and not any of '" + yogList[rule].enhanceif.disposition + "']");
                                break;
                        }

                    });
                    break;
                case "excludeif":
                    Object.keys(yogList[rule].excludeif).forEach((exi) => {
                        switch (exi) {
                            case "disposition":
                                var anyOf = false;
                                if (Array.isArray(yogList[rule].excludeif.disposition)) yogList[rule].excludeif.disposition.forEach((de) => anyOf = anyOf || disp.get(p1).includes(de));
                                else if (disp.get(p1).includes(yogList[rule].excludeif.disposition)) anyOf = true;
                                if (!anyOf) ruleElementPassed++;
                                else mismatch.push("> EXCLUDED disposition - [" + p1 + " is '" + yogList[rule].excludeif.disposition + "']");
                                break;
                        }
                    });
                    break;
            }
        });
        if (ruleElementPassed >= el2pass) yogsFound.set(yogList[rule].title, JSON.stringify(yogList[rule]));
        else console.log("---" + yogList[rule].title + " failed due to " + ruleElementPassed + " of " + el2pass + " matches [" + mismatch + "]");
    }
    
    return yogsFound;

};
//export {yogList};